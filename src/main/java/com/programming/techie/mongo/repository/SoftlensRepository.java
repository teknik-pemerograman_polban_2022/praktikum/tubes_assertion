package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.programming.techie.mongo.model.Softlens;

public interface SoftlensRepository extends MongoRepository<Softlens, String> {
    @Query("{'nama': ?0}")
    Softlens findByName(String nama);

    default int sumStock() {
        return findAll()
                .stream()
                .mapToInt(Softlens::getStokSoftlens)
                .sum();
    }
}
