package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.programming.techie.mongo.model.Pembelian;

public interface PembelianRepository extends MongoRepository<Pembelian, String> {
  @Query("{'Pembeli': ?0}")
  Pembelian findByName(String Pembeli);

  default int GetNewNoPembelian() {
    return (int) findAll().stream().count() + 1;
  }
}
