package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.programming.techie.mongo.model.BPJS;

public interface BPJSRepository extends MongoRepository<BPJS, String> {
  @Query("{'Peserta': ?0}")
  BPJS findByName(String Peserta);

  default int GetNewNoBPJS() {
    return (int) findAll().stream().count() + 1;
  }
}
