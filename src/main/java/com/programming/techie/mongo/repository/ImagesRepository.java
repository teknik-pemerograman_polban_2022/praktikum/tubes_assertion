package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.programming.techie.mongo.model.Images;

public interface ImagesRepository extends MongoRepository<Images, String> {

}
