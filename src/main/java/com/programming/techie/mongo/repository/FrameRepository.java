package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.programming.techie.mongo.model.Frame;

public interface FrameRepository extends MongoRepository<Frame, String> {
    @Query("{'nama': ?0}")
    Frame findByName(String name);

    default int sumStock() {
        return findAll()
                .stream()
                .mapToInt(Frame::getStokFrame)
                .sum();
    }
}
