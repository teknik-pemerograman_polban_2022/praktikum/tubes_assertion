package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.programming.techie.mongo.model.Box;

public interface BoxRepository extends MongoRepository<Box, String> {
    @Query("{'nama': ?0}")
    Box findByName(String nama);

    default int sumStock() {
        return findAll()
                .stream()
                .mapToInt(Box::getStokBox)
                .sum();
    }
}
