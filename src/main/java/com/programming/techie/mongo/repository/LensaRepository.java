package com.programming.techie.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.programming.techie.mongo.model.Lensa;

public interface LensaRepository extends MongoRepository<Lensa, String> {
    @Query("{'nama': ?0}")
    Lensa findByName(String nama);

    default int sumStock() {
        return findAll()
                .stream()
                .mapToInt(Lensa::getStokLensa)
                .sum();
    }
}
