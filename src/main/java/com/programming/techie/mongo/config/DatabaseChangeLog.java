package com.programming.techie.mongo.config;

import static com.programming.techie.mongo.model.CategoriLensa.Single_Vision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.programming.techie.mongo.model.Box;
import com.programming.techie.mongo.model.CategoriBox;
import com.programming.techie.mongo.model.CategoriFrame;
import com.programming.techie.mongo.model.CategoriLensa;
import com.programming.techie.mongo.model.Frame;
import com.programming.techie.mongo.model.Lensa;
import com.programming.techie.mongo.model.Softlens;
import com.programming.techie.mongo.repository.BoxRepository;
import com.programming.techie.mongo.repository.FrameRepository;
import com.programming.techie.mongo.repository.LensaRepository;
import com.programming.techie.mongo.repository.SoftlensRepository;

@ChangeLog
public class DatabaseChangeLog {

        @ChangeSet(order = "001", id = "seedDatabase", author = "Sai")
        public void seedDatabase(LensaRepository lensaRepository, FrameRepository frameRepository,
                        BoxRepository boxRepository, SoftlensRepository softlensRepository) {
                List<Lensa> LensaList = new ArrayList<>();
                LensaList.add(createNewLensa("Blueray", Single_Vision, 1, 1, 0, "2023-01-01", new BigDecimal(250000)));
                LensaList.add(
                                createNewLensa("Essilor", CategoriLensa.Progressive, 1, 2, 100, "2020-08-10",
                                                new BigDecimal(200000)));
                LensaList.add(
                                createNewLensa("Rodenstock", CategoriLensa.Double_Focus, 2, 2, 5, "2023-02-02",
                                                new BigDecimal(3000000)));
                LensaList.add(createNewLensa("Calvin Klein Lens", CategoriLensa.Double_Focus, 2, 3, 8, "2023-02-02",
                                new BigDecimal(400000)));
                LensaList.add(
                                createNewLensa("Oakley", CategoriLensa.Double_Focus, 3, 1, 80, "2023-04-02",
                                                new BigDecimal(400000)));
                LensaList.add(
                                createNewLensa("Zeiss", CategoriLensa.Double_Focus, 1, 3, 70, "2023-01-02",
                                                new BigDecimal(100000)));

                List<Frame> FrameList = new ArrayList<>();
                FrameList.add(createNewFrame("Oakley", CategoriFrame.Plastik, 10, "2023-01-01",
                                new BigDecimal(1299000)));
                FrameList.add(createNewFrame("Calvin", CategoriFrame.Plastik, 20, "2023-01-01",
                                new BigDecimal(3500000)));
                FrameList.add(createNewFrame("Chanel", CategoriFrame.Besi, 2, "2023-01-01", new BigDecimal(6500000)));
                FrameList.add(createNewFrame("Guess", CategoriFrame.Plastik, 5, "2023-01-01", new BigDecimal(2010000)));

                List<Box> BoxList = new ArrayList<>();
                BoxList.add(createNewBox("Xcent", CategoriBox.Default, 1000, "2023-01-01", new BigDecimal(0)));
                BoxList.add(createNewBox("OakPremiun", CategoriBox.Custom, 100, "2023-01-01", new BigDecimal(700000)));

                List<Softlens> SoftlensList = new ArrayList<>();
                SoftlensList.add(createNewSoftLens("Roudent", 10, "Kuning", 9, "2023-01-01", new BigDecimal(100000)));
                SoftlensList.add(createNewSoftLens("Youj", 10, "Kuning", 9, "2023-01-01", new BigDecimal(100000)));

                lensaRepository.insert(LensaList);
                frameRepository.insert(FrameList);
                boxRepository.insert(BoxList);
                softlensRepository.insert(SoftlensList);
        }

        private Lensa createNewLensa(String NamaLensa, CategoriLensa JenisLensa, float UkuranKiri, float UkuranKanan,
                        int StokLensa, String TglMasuk,
                        BigDecimal Harga) {
                Lensa lensa = new Lensa();

                lensa.setNamaLensa(NamaLensa);
                lensa.setJenisLensa(JenisLensa);
                lensa.setUkuranKiri(UkuranKiri);
                lensa.setUkuranKanan(UkuranKanan);
                lensa.setStokLensa(StokLensa);
                lensa.setTglMasuk(TglMasuk);
                lensa.setHarga(Harga);

                return lensa;
        }

        private Frame createNewFrame(String NamaFrame, CategoriFrame Jenis, int Stok, String tglMasuk,
                        BigDecimal Harga) {
                Frame frame = new Frame();
                frame.setNamaFrame(NamaFrame);
                frame.setJenisFrame(Jenis);
                frame.setStokFrame(Stok);
                frame.setStokFrame(Stok);
                frame.setTglMasuk(tglMasuk);
                frame.setHarga(Harga);
                return frame;
        }

        private Box createNewBox(String NamaBox, CategoriBox Jenis, int Stock, String TglMasuk, BigDecimal Harga) {
                Box box = new Box();

                box.setNamaBox(NamaBox);
                box.setJenisBox(Jenis);
                box.setStokBox(Stock);
                box.setTglMasuk(TglMasuk);
                box.setHarga(Harga);

                return box;
        }

        private Softlens createNewSoftLens(String nama, float ukuran, String warna, int stok, String tglMasuk,
                        BigDecimal harga) {
                Softlens softlens = new Softlens();

                softlens.setNamaSoftlens(nama);
                softlens.setUkuranSoftlens(ukuran);
                softlens.setWarnaSoftlens(warna);
                softlens.setStokSoftlens(stok);
                softlens.setTglMasuk(tglMasuk);
                softlens.setHarga(harga);

                return softlens;
        }
}
