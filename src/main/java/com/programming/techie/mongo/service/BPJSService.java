package com.programming.techie.mongo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.programming.techie.mongo.model.BPJS;
import com.programming.techie.mongo.repository.BPJSRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class BPJSService {

    private final BPJSRepository BPJSRepository;

    public void addBPJS(BPJS BPJS) {

        BPJS.setNoBPJS(BPJSRepository.GetNewNoBPJS());

        BPJSRepository.insert(BPJS);
    }

    public void updateBPJS(BPJS BPJS) {

        BPJS savedBPJS = BPJSRepository.findById(BPJS.getId()).orElseThrow(
                () -> new RuntimeException(String.format("Cannot Find data BPJS by ID %s", BPJS.getId())));

        BPJS.setNoBPJS(savedBPJS.getNoBPJS());

        BPJSRepository.save(BPJS);
    }

    public BPJS getBPJS(String Peserta) {
        return BPJSRepository.findByName(Peserta);
    }

    public BPJS getBPJSById(String id) {
        return BPJSRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("BPJS not found for id: " + id));
    }

    public List<BPJS> getAllBPJS() {
        return BPJSRepository.findAll();
    }

    public void deleteBPJS(String id) {
        BPJSRepository.deleteById(id);
    }
}
