package com.programming.techie.mongo.service;

import com.programming.techie.mongo.model.Lensa;
import com.programming.techie.mongo.repository.LensaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class LensaService {

    private final LensaRepository LensaRepository;

    public void addLensa(Lensa lensa) {
        LensaRepository.insert(lensa);
    }

    public void updateLensa(Lensa lensa) {
        LensaRepository.save(lensa);
    }

    public Lensa getLensa(String nama) {
        return LensaRepository.findByName(nama);
    }

    public Lensa getLensaById(String id) {
        return LensaRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Lensa not found for id: " + id));
    }

    public List<Lensa> getAllLensa() {
        return LensaRepository.findAll();
    }

    public void deleteLensa(String id) {
        LensaRepository.deleteById(id);
    }

    public int sumStock() {
        return LensaRepository.sumStock();
    }
}
