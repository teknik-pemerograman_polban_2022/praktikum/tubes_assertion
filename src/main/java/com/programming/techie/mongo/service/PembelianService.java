package com.programming.techie.mongo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.programming.techie.mongo.model.Box;
import com.programming.techie.mongo.model.Frame;
import com.programming.techie.mongo.model.Lensa;
import com.programming.techie.mongo.model.Pembelian;
import com.programming.techie.mongo.repository.BoxRepository;
import com.programming.techie.mongo.repository.FrameRepository;
import com.programming.techie.mongo.repository.LensaRepository;
import com.programming.techie.mongo.repository.PembelianRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class PembelianService {

    private final PembelianRepository PembelianRepository;

    private final LensaService LensaService;
    private final FrameService FrameService;
    private final BoxService BoxService;

    public void addPembelian(Pembelian Pembelian) {

        Lensa FindLensa = LensaService.getLensaById(Pembelian.getIdLensa());
        Box FindBox = BoxService.getBoxById(Pembelian.getIdBox());
        Frame FindFrame = FrameService.getFrameById(Pembelian.getIdFrame());

        Pembelian.setNoPembelian(PembelianRepository.GetNewNoPembelian());
        Pembelian.setIdBox(FindBox.getId());
        Pembelian.setIdFrame(FindFrame.getId());
        Pembelian.setIdLensa(FindLensa.getId());

        FindLensa.setStokLensa(FindLensa.getStokLensa() - 1);
        FindBox.setStokBox(FindBox.getStokBox() - 1);
        FindFrame.setStokFrame(FindBox.getStokBox() - 1);
        LensaService.updateLensa(FindLensa);
        FrameService.updateFrame(FindFrame);
        BoxService.updateBox(FindBox);

        PembelianRepository.insert(Pembelian);
    }

    public void updatePembelian(Pembelian Pembelian) {

        Pembelian savedPembelian = PembelianRepository.findById(Pembelian.getId()).orElseThrow(
                () -> new RuntimeException(String.format("Cannot Find data pembelian by ID %s", Pembelian.getId())));

        Lensa FindLensa = LensaService.getLensaById(Pembelian.getIdLensa());
        Box FindBox = BoxService.getBoxById(Pembelian.getIdBox());
        Frame FindFrame = FrameService.getFrameById(Pembelian.getIdFrame());

        Lensa OldLensa = LensaService.getLensaById(savedPembelian.getIdLensa());
        Box OldBox = BoxService.getBoxById(savedPembelian.getIdBox());
        Frame OldFrame = FrameService.getFrameById(savedPembelian.getIdFrame());

        savedPembelian.setKategoriPembeli(Pembelian.getKategoriPembeli());
        savedPembelian.setPembeli(Pembelian.getPembeli());
        savedPembelian.setDrPemeriksa(Pembelian.getDrPemeriksa());
        Pembelian.setIdBox(FindBox.getId());
        Pembelian.setIdFrame(FindFrame.getId());
        Pembelian.setIdLensa(FindLensa.getId());
        savedPembelian.setTanggalSelesai(Pembelian.getTanggalSelesai());
        savedPembelian.setUangMuka(Pembelian.getUangMuka());

        FindLensa.setStokLensa(FindLensa.getStokLensa() - 1);
        FindBox.setStokBox(FindBox.getStokBox() - 1);
        FindFrame.setStokFrame(FindBox.getStokBox() - 1);
        LensaService.updateLensa(FindLensa);
        FrameService.updateFrame(FindFrame);
        BoxService.updateBox(FindBox);

        OldLensa.setStokLensa(OldLensa.getStokLensa() + 1);
        OldBox.setStokBox(OldBox.getStokBox() + 1);
        OldFrame.setStokFrame(OldBox.getStokBox() + 1);
        LensaService.updateLensa(OldLensa);
        FrameService.updateFrame(OldFrame);
        BoxService.updateBox(OldBox);

        PembelianRepository.save(Pembelian);
    }

    public Pembelian getPembelian(String Pembeli) {
        return PembelianRepository.findByName(Pembeli);
    }

    public Pembelian getPembelianById(String id) {
        return PembelianRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Pembelian not found for id: " + id));
    }

    public List<Pembelian> getAllPembelian() {
        return PembelianRepository.findAll();
    }

    public void deletePembelian(String id) {
        PembelianRepository.deleteById(id);
    }
}
