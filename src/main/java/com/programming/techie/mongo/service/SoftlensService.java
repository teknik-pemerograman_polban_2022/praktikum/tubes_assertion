package com.programming.techie.mongo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.programming.techie.mongo.model.Softlens;
import com.programming.techie.mongo.repository.SoftlensRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class SoftlensService {

    private final SoftlensRepository SoftlensRepository;

    public void addSoftlens(Softlens softlens) {
        SoftlensRepository.insert(softlens);
    }

    public void updateSoftlens(Softlens softlens) {
        SoftlensRepository.save(softlens);
    }

    public Softlens getSoftlens(String nama) {
        return SoftlensRepository.findByName(nama);
    }

    public List<Softlens> getAllSoftlens() {
        return SoftlensRepository.findAll();
    }

    public void deleteSoftlens(String id) {
        SoftlensRepository.deleteById(id);
    }

    public int sumStock() {
        return SoftlensRepository.sumStock();
    }
}
