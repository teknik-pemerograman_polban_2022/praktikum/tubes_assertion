package com.programming.techie.mongo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.programming.techie.mongo.model.Box;
import com.programming.techie.mongo.repository.BoxRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class BoxService {

    private final BoxRepository BoxRepository;

    public void addBox(Box box) {
        BoxRepository.insert(box);
    }

    public void updateBox(Box box) {
        BoxRepository.save(box);
    }

    public Box getBox(String nama) {
        return BoxRepository.findByName(nama);
    }

    public Box getBoxById(String id) {
        return BoxRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Box not found for id: " + id));
    }

    public List<Box> getAllBox() {
        return BoxRepository.findAll();
    }

    public void deleteBox(String id) {
        BoxRepository.deleteById(id);
    }

    public int sumStock() {
        return BoxRepository.sumStock();
    }
}
