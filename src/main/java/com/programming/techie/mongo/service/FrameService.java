package com.programming.techie.mongo.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.programming.techie.mongo.model.Frame;
import com.programming.techie.mongo.repository.FrameRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class FrameService {

    private final FrameRepository FrameRepository;

    public void addFrame(Frame frame) {
        FrameRepository.insert(frame);
    }

    public void updateFrame(Frame frame) {
        FrameRepository.save(frame);
    }

    public Frame getFrame(String name) {
        return FrameRepository.findByName(name);
    }

    public Frame getFrameById(String id) {
        return FrameRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Frame not found for id: " + id));
    }

    public List<Frame> getAllFrame() {
        return FrameRepository.findAll();
    }

    public void deleteFrame(String id) {
        FrameRepository.deleteById(id);
    }

    public int sumStock() {
        return FrameRepository.sumStock();
    }
}
