package com.programming.techie.mongo.model;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("Lensa")
public class Lensa {
    @Id
    private String id;
    @NotNull
    @Field("nama")
    private String NamaLensa;
    @NotNull
    @Field("jenis")
    private CategoriLensa JenisLensa;
    @NotNull
    @Min(value = 0)
    @Field("ukuranL")
    private Float UkuranKiri;
    @NotNull
    @Min(value = 0)
    @Field("ukuranR")
    private Float UkuranKanan;
    @NotNull
    @Digits(integer = 10, fraction = 0)
    @Field("stock")
    private int StokLensa;
    @NotNull
    @Field("tglMasuk")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String TglMasuk;
    @NotNull
    @Field("harga")
    private BigDecimal Harga;
}
