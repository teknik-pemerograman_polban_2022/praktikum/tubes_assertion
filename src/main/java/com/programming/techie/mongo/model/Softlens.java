package com.programming.techie.mongo.model;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("Softlens")
public class Softlens {
    @Id
    private String id;
    @Field("imageID")
    private String Imageid;
    @NotNull
    @Field("nama")
    private String NamaSoftlens;
    @NotNull
    @Min(value = 0)
    @Field("ukuran")
    private Float UkuranSoftlens;
    @NotNull
    @Field("warna")
    private String WarnaSoftlens;
    @NotNull
    @Min(value = 0)
    @Field("stock")
    private int StokSoftlens;
    @NotNull
    @Field("tglMasuk")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String TglMasuk;
    @NotNull
    @Field("harga")
    private BigDecimal Harga;
}
