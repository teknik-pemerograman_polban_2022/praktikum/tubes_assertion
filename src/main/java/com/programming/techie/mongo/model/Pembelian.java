package com.programming.techie.mongo.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("Pembelian")
public class Pembelian {
    @Id
    private String id;
    private int noPembelian;
    @NotNull
    private CategoriPembeli KategoriPembeli;
    @NotNull
    private String Pembeli;
    @NotNull
    private String DrPemeriksa;
    @NotNull
    private String idLensa;
    @NotNull
    private String idFrame;
    @NotNull
    private String idBox;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String TanggalSelesai;
    @NotNull
    private BigDecimal UangMuka;

    @LastModifiedDate
    private LocalDateTime updatedAt;

    @Transient
    private Lensa lensa;
    @Transient
    private Box box;
    @Transient
    private Frame frame;
}
