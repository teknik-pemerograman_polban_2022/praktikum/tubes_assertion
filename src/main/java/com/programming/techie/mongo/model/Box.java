package com.programming.techie.mongo.model;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("Box")
public class Box {
    @Id
    private String id;
    @NotNull
    @Field("nama")
    private String NamaBox;
    @NotNull
    @Field("jenis")
    private CategoriBox JenisBox;
    @NotNull
    @Min(value = 0)
    @Field("stock")
    private int StokBox;
    @NotNull
    @Field("tglMasuk")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String TglMasuk;
    @NotNull
    @Field("harga")
    private BigDecimal Harga;
}
