package com.programming.techie.mongo.model;

public enum CategoriLensa {
    Single_Vision, Double_Focus, Progressive
}