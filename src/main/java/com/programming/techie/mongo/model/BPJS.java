package com.programming.techie.mongo.model;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("BPJS")
public class BPJS {
    @Id
    private String id;
    @NotNull
    private int noBPJS;
    @NotNull
    private String Peserta;
    @NotNull
    @Digits(integer = 13, fraction = 0)
    private String noKartuPeserta;
    @NotNull
    private String AsalResep;
    @NotNull
    private float UkuranKacamata_kiri;
    @NotNull
    private float UkuranKacamata_kanan;
    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String Tanggal;

}
