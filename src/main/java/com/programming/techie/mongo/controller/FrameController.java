package com.programming.techie.mongo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.programming.techie.mongo.model.Frame;
import com.programming.techie.mongo.service.FrameService;

import lombok.RequiredArgsConstructor;

import org.springframework.validation.BindingResult;

@Controller
@RequestMapping("/frame")
@RequiredArgsConstructor
public class FrameController extends BaseController {

    private final FrameService frameService;
    private final ImagesController imageController;

    @Override
    protected String getTemplateName() {
        return "Lensa/Frame";
    }

    @Override
    protected void populateModel(Model model) {
        List<Frame> dataFrame = frameService.getAllFrame();
        model.addAttribute("DataFrame", dataFrame);
        model.addAttribute("DataNamaFrame", dataFrame);
        model.addAttribute("form", new Frame());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addFrame(@ModelAttribute @Valid Frame frame, BindingResult result,
            @RequestParam("image") MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", frame);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data frame. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/frame";
        }

        frame.setImageid(imageController.uploadImage(file));
        frameService.addFrame(frame);
        return "redirect:/frame";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateFrame(@ModelAttribute @Valid Frame frame, BindingResult result,
            @RequestParam("image") MultipartFile file, RedirectAttributes redirectAttributes)
            throws IOException {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", frame);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat mengubah data frame. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/frame";
        }

        frame.setImageid(imageController.uploadImage(file));
        frameService.updateFrame(frame);
        return "redirect:/frame";
    }

    @GetMapping("/{nama}")
    public String getFrameByName(@PathVariable String nama, Model model) {
        List<Frame> dataLensa = new ArrayList<Frame>();
        dataLensa.add(frameService.getFrame(nama));
        List<Frame> dataNamaLensa = frameService.getAllFrame();
        model.addAttribute("DataNamaFrame", dataNamaLensa);
        model.addAttribute("DataFrame", dataLensa);
        model.addAttribute("Search", nama);
        return "Lensa/Frame";
    }

}
