package com.programming.techie.mongo.controller;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.programming.techie.mongo.model.Images;
import com.programming.techie.mongo.repository.ImagesRepository;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/image")
@RequiredArgsConstructor
public class ImagesController {

    @Autowired
    private ImagesRepository imageRepository;

    @PostMapping("/uploadImage")
    public String uploadImage(@RequestParam("image") MultipartFile file) throws IOException {
        Images image = new Images();
        image.setName(file.getOriginalFilename());
        image.setContentType(file.getContentType());
        image.setContent(file.getBytes());
        imageRepository.save(image);
        return image.getId();
    }

    @GetMapping("/getImage/{id}")
    public void getImage(@PathVariable String id, HttpServletResponse response) throws IOException {
        Images image = imageRepository.findById(id).orElse(null);
        if (image != null) {
            response.setContentType(image.getContentType());
            response.getOutputStream().write(image.getContent());
        }
    }

}
