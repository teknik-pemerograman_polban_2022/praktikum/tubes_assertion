package com.programming.techie.mongo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.programming.techie.mongo.service.BoxService;
import com.programming.techie.mongo.service.FrameService;
import com.programming.techie.mongo.service.LensaService;
import com.programming.techie.mongo.service.SoftlensService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class MainController {

    private final FrameService frameService;
    private final LensaService lensaService;
    private final BoxService boxService;
    private final SoftlensService softlenseService;

    @GetMapping
    public String getAllFrame(Model model) {
        // List<Frame> DataFrame = frameService.getAllFrame();
        // model.addAttribute("DataFrame", DataFrame);
        // model.addAttribute("DataNamaFrame", DataFrame);
        model.addAttribute("jmlLensa", lensaService.sumStock());
        model.addAttribute("jmlFrame", frameService.sumStock());
        model.addAttribute("jmlBox", boxService.sumStock());
        model.addAttribute("jmlSoftlens", softlenseService.sumStock());
        return "Lensa/index";
    }

}
