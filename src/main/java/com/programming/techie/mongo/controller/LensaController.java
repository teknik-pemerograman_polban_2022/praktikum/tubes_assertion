package com.programming.techie.mongo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.programming.techie.mongo.model.Lensa;
import com.programming.techie.mongo.service.LensaService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/lensa")
@RequiredArgsConstructor
public class LensaController extends BaseController {

    private final LensaService lensaService;

    @Override
    protected String getTemplateName() {
        return "Lensa/Lensa";
    }

    @Override
    protected void populateModel(Model model) {
        List<Lensa> DataLensa = lensaService.getAllLensa();
        model.addAttribute("DataLensa", DataLensa);
        model.addAttribute("DataNamaLensa", DataLensa);
        model.addAttribute("form", new Lensa());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addLensa(@ModelAttribute @Valid Lensa lensa, BindingResult result,
            RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", lensa);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data lensa. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/lensa";
        }

        lensaService.addLensa(lensa);
        return "redirect:/lensa";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateLensa(@ModelAttribute @Valid Lensa lensa, BindingResult result,
            RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", lensa);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data lensa. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/lensa";
        }

        lensaService.updateLensa(lensa);
        return "redirect:/lensa";
    }

    @GetMapping("/{nama}")
    public String getLensaByName(@PathVariable String nama, Model model) {
        List<Lensa> DataLensa = new ArrayList<Lensa>();
        DataLensa.add(lensaService.getLensa(nama));
        List<Lensa> DataNamaLensa = lensaService.getAllLensa();
        model.addAttribute("DataNamaLensa", DataNamaLensa);
        model.addAttribute("DataLensa", DataLensa);
        model.addAttribute("Search", nama);
        return "Lensa/Lensa";
    }

}
