package com.programming.techie.mongo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.programming.techie.mongo.model.Box;
import com.programming.techie.mongo.model.Frame;
import com.programming.techie.mongo.model.Lensa;
import com.programming.techie.mongo.model.Pembelian;
import com.programming.techie.mongo.service.BoxService;
import com.programming.techie.mongo.service.FrameService;
import com.programming.techie.mongo.service.LensaService;
import com.programming.techie.mongo.service.PembelianService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/pembelian")
@RequiredArgsConstructor
public class PembelianController extends BaseController {

    private final PembelianService PembelianService;
    private final LensaService LensaService;
    private final FrameService FrameService;
    private final BoxService BoxService;

    @Override
    protected String getTemplateName() {
        return "Lensa/Pembelian";
    }

    @Override
    protected void populateModel(Model model) {
        List<Pembelian> DataPembelian = PembelianService.getAllPembelian();
        for (Pembelian pembelian : DataPembelian) {
            Lensa lensa = LensaService.getLensaById(pembelian.getIdLensa());
            Box box = BoxService.getBoxById(pembelian.getIdBox());
            Frame frame = FrameService.getFrameById(pembelian.getIdFrame());
            pembelian.setLensa(lensa);
            pembelian.setFrame(frame);
            pembelian.setBox(box);
        }
        List<Lensa> DataLensa = LensaService.getAllLensa();
        List<Frame> DataFrame = FrameService.getAllFrame();
        List<Box> DataBox = BoxService.getAllBox();
        model.addAttribute("DataPembelian", DataPembelian);
        model.addAttribute("DataLensa", DataLensa);
        model.addAttribute("DataFrame", DataFrame);
        model.addAttribute("DataBox", DataBox);
        model.addAttribute("form", new Pembelian());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPembelian(@ModelAttribute @Valid Pembelian Pembelian, BindingResult result,
            RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", Pembelian);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data pembelian. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/pembelian";
        }

        PembelianService.addPembelian(Pembelian);
        return "redirect:/pembelian";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updatePembelian(@ModelAttribute @Valid Pembelian Pembelian, BindingResult result,
            RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", Pembelian);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data pembelian. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/pembelian";
        }

        PembelianService.updatePembelian(Pembelian);
        return "redirect:/pembelian";
    }

    @GetMapping("/{nama}")
    public String getPembelianByName(@PathVariable String nama, Model model) {
        List<Pembelian> DataPembelian = new ArrayList<Pembelian>();
        DataPembelian.add(PembelianService.getPembelian(nama));
        for (Pembelian pembelian : DataPembelian) {
            Lensa lensa = LensaService.getLensaById(pembelian.getIdLensa());
            Box box = BoxService.getBoxById(pembelian.getIdBox());
            Frame frame = FrameService.getFrameById(pembelian.getIdFrame());
            pembelian.setLensa(lensa);
            pembelian.setFrame(frame);
            pembelian.setBox(box);
        }
        model.addAttribute("DataPembelian", DataPembelian);
        model.addAttribute("Search", nama);
        List<Lensa> DataLensa = LensaService.getAllLensa();
        List<Frame> DataFrame = FrameService.getAllFrame();
        List<Box> DataBox = BoxService.getAllBox();
        model.addAttribute("DataLensa", DataLensa);
        model.addAttribute("DataFrame", DataFrame);
        model.addAttribute("DataBox", DataBox);
        return "Lensa/Pembelian";
    }

    @GetMapping("/print/{id}")
    public String printPembelian(@PathVariable String id, Model model) {
        Pembelian DataPembelian = PembelianService.getPembelianById(id);
        Lensa lensa = LensaService.getLensaById(DataPembelian.getIdLensa());
        Box box = BoxService.getBoxById(DataPembelian.getIdBox());
        Frame frame = FrameService.getFrameById(DataPembelian.getIdFrame());
        DataPembelian.setLensa(lensa);
        DataPembelian.setFrame(frame);
        DataPembelian.setBox(box);
        model.addAttribute("Data", DataPembelian);
        return "Printable/Kwitansi/index";
    }
}
