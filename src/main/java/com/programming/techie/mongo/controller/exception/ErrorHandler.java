package com.programming.techie.mongo.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(value = { BadRequestException.class, ResourceNotFoundException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleBadRequestException(Model model, Exception ex) {
        model.addAttribute("status", 400);
        model.addAttribute("error", "Permintaan Kamu Tidak Dapat Di Proses");
        return "error";
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleAnyException(Throwable ex, Model model) {
        model.addAttribute("status", 400);
        model.addAttribute("error", "Server Kami Mengalami Masalah");
        return "error";
    }
}
