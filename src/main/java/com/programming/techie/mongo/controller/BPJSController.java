package com.programming.techie.mongo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.programming.techie.mongo.model.BPJS;
import com.programming.techie.mongo.service.BPJSService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/bpjs")
@RequiredArgsConstructor
public class BPJSController extends BaseController {

    private final BPJSService bpjsService;

    @Override
    protected String getTemplateName() {
        return "Lensa/BPJS";
    }

    @Override
    protected void populateModel(Model model) {
        List<BPJS> DataBPJS = bpjsService.getAllBPJS();
        model.addAttribute("DataBPJS", DataBPJS);
        model.addAttribute("DataNamaBPJS", DataBPJS);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addBPJS(@ModelAttribute @Valid BPJS bpjs, BindingResult result,
            RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", bpjs);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data bpjs. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/bpjs";
        }
        bpjsService.addBPJS(bpjs);
        return "redirect:/bpjs";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateBPJS(@ModelAttribute @Valid BPJS bpjs, BindingResult result,
            RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", bpjs);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data bpjs. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/bpjs";
        }
        bpjsService.updateBPJS(bpjs);
        return "redirect:/bpjs";
    }

    @GetMapping("/{nama}")
    public String getBPJSByName(@PathVariable String nama, Model model) {
        List<BPJS> DataBPJS = new ArrayList<BPJS>();
        DataBPJS.add(bpjsService.getBPJS(nama));
        List<BPJS> DataNamaBPJS = bpjsService.getAllBPJS();
        model.addAttribute("DataNamaBPJS", DataNamaBPJS);
        model.addAttribute("DataBPJS", DataBPJS);
        model.addAttribute("Search", nama);
        return "Lensa/BPJS";
    }

    @GetMapping("/delete/{id}")
    public String deleteBPJSByName(@PathVariable String id, Model model) {
        List<BPJS> DataBPJS = new ArrayList<BPJS>();
        DataBPJS.add(bpjsService.getBPJS(id));
        List<BPJS> DataNamaBPJS = bpjsService.getAllBPJS();
        model.addAttribute("DataNamaBPJS", DataNamaBPJS);
        model.addAttribute("DataBPJS", DataBPJS);
        model.addAttribute("Search", id);
        return "Lensa/BPJS";
    }
}