package com.programming.techie.mongo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.programming.techie.mongo.model.Box;
import com.programming.techie.mongo.service.BoxService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/box")
@RequiredArgsConstructor
public class BoxController extends BaseController {

    private final BoxService boxService;

    @Override
    protected String getTemplateName() {
        return "Lensa/Box";
    }

    @Override
    protected void populateModel(Model model) {
        List<Box> DataBox = boxService.getAllBox();
        model.addAttribute("DataBox", DataBox);
        model.addAttribute("DataNamaBox", DataBox);
        model.addAttribute("form", new Box());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addBox(@ModelAttribute @Valid Box box, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", box);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data box. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/box";
        }

        boxService.addBox(box);
        return "redirect:/box";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateBox(@ModelAttribute @Valid Box box, BindingResult result,
            RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", box);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data box. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/box";
        }
        boxService.updateBox(box);
        return "redirect:/box";
    }

    @GetMapping("/{nama}")
    public String getBoxByName(@PathVariable String nama, Model model) {
        List<Box> DataBox = new ArrayList<Box>();
        DataBox.add(boxService.getBox(nama));
        List<Box> DataNamaBox = boxService.getAllBox();
        model.addAttribute("DataNamaBox", DataNamaBox);
        model.addAttribute("DataBox", DataBox);
        model.addAttribute("Search", nama);
        return "Lensa/Box";
    }

}
