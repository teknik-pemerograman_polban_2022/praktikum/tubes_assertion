package com.programming.techie.mongo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.programming.techie.mongo.model.Softlens;
import com.programming.techie.mongo.service.SoftlensService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/softlens")
@RequiredArgsConstructor
public class SoftlensController extends BaseController {

    private final SoftlensService softlensService;
    private final ImagesController imageController;

    @Override
    protected String getTemplateName() {
        return "Lensa/Softlens";
    }

    @Override
    protected void populateModel(Model model) {
        List<Softlens> DataSoftlens = softlensService.getAllSoftlens();
        model.addAttribute("DataSoftlens", DataSoftlens);
        model.addAttribute("DataNamaSoftlens", DataSoftlens);
        model.addAttribute("form", new Softlens());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSoftlens(@ModelAttribute @Valid Softlens softlens, BindingResult result,
            @RequestParam("image") MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", softlens);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data softlens. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/softlens";
        }

        softlens.setImageid(imageController.uploadImage(file));
        softlensService.addSoftlens(softlens);
        return "redirect:/softlens";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateSoftlens(@ModelAttribute @Valid Softlens softlens, BindingResult result,
            @RequestParam("image") MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", softlens);
            redirectAttributes.addFlashAttribute("messages",
                    "Terjadi kesalahan saat menambahkan data softlens. Silahkan cek kembali isian Anda.");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/softlens";
        }

        softlens.setImageid(imageController.uploadImage(file));
        softlensService.updateSoftlens(softlens);
        return "redirect:/softlens";
    }

    @GetMapping("/{nama}")
    public String getSoftlensByName(@PathVariable String nama, Model model) {
        List<Softlens> DataSoftlens = new ArrayList<Softlens>();
        DataSoftlens.add(softlensService.getSoftlens(nama));
        List<Softlens> DataNamaSoftlens = softlensService.getAllSoftlens();
        model.addAttribute("DataNamaSoftlens", DataNamaSoftlens);
        model.addAttribute("DataSoftlens", DataSoftlens);
        model.addAttribute("Search", nama);
        return "Lensa/Softlens";
    }

}
